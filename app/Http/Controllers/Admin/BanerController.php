<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Baner;
use Illuminate\Http\Request;

class BanerController extends Controller
{
    public function list()
    {
        $data = Baner::orderBy('created_at', 'DESC')->where('trans_id', null)->get();
        return view('admin.pages.list')->withData($data)->withType('baners');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')){
            $validation = [
                'title' => 'required|string',
                'image' => 'required|string',
                'short' => 'required|string',
                'link' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data = new Baner;
            $data->title = $request->title;
            $data->image = $request->image;
            $data->short = $request->short;
            $data->link = $request->link;
            $data->order = $request->order;
            $data->lang = 'ru';
            $data->save();

            return redirect()->route('baners')->with('message', 'Successfully created');

        }
        $items = [
            'title', 'image', 'short', 'link', 'lang', 'order'
        ];
        return view('admin.pages.action')->withType('baners')->withAction('create')->withItems($items);
    }

    public function edit(Request $request, $id)
    {
        $data = Baner::find($id);
        if ($request->isMethod('post')){
            $validation = [
                'title' => 'required|string',
                'image' => 'required|string',
                'short' => 'required|string',
                'link' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data->image = $request->image;
            $data->title = $request->title;
            $data->short = $request->short;
            $data->link = $request->link;
            $data->order = $request->order;
            $data->save();

            return redirect()->route('baners')->with('message', 'Successfully edited');

        }

        $items = [
            'title', 'image', 'short', 'link', 'lang', 'order'
        ];
        return view('admin.pages.action')->withType('baners')->withAction('edit')->withItems($items)->withData($data);
    }

    public function translate(Request $request, $id, $lang)
    {
        $parent = Baner::find($id);
        $data = Baner::where('trans_id', $id)->where('lang', $lang)->first();
        if (!$data){
            $data = new Baner;
        }
        if ($request->isMethod('post')){
            $validation = [
                'title' => 'required|string',
                'short' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data->title = $request->title;
            $data->short = $request->short;
            $data->link = $parent->link;
            $data->order = $parent->order;
            $data->image = $parent->image;
            $data->trans_id = $id;
            $data->lang = $lang;
            $data->save();

            return redirect()->route('baners')->with('message', 'Successfully translated');

        }

        $items = [
            'title', 'short', 'lang'
        ];

        return view('admin.pages.action')->withType('baners')->withAction('translate')->withItems($items)->withData($data)->withLang($lang)->withId($id);
    }

    public function delete($id)
    {
        $data = Baner::find($id);
        Baner::where('trans_id', $id)->delete();
        $data->delete();
        return redirect()->back()->with('message', 'Successfully deleted');
    }
}
