<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function list(Request $request)
    {
        $data = Category::orderBy('order')->where('parent_id', null)->get();
        return view('admin.pages.categories')->withMenu($data);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $validation = [
                'title_ru' => 'required|string',
                'title_en' => 'required|string',
                'title_uz' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data = new Category;
            $data->title_ru = $request->title_ru;
            $data->title_en = $request->title_en;
            $data->title_uz = $request->title_uz;
            $data->save();

            return redirect()->route('category')->with('message', 'Successfully created');

        }
        $items = [
            'title_uz', 'title_en', 'title_ru'
        ];
        return view('admin.pages.action')->withType('category')->withAction('create')->withItems($items);
    }

    public function edit(Request $request, $id)
    {
        $data = Category::find($id);
        if ($request->isMethod('post')) {
            $validation = [
                'title_ru' => 'required|string',
                'title_en' => 'required|string',
                'title_uz' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data->title_ru = $request->title_ru;
            $data->title_en = $request->title_en;
            $data->title_uz = $request->title_uz;
            $data->save();

            return redirect()->route('category')->with('message', 'Successfully edited');

        }

        $items = [
            'title_uz', 'title_en', 'title_ru'
        ];
        return view('admin.pages.action')->withType('category')->withAction('edit')->withItems($items)->withData($data);
    }

    public function delete($id)
    {
        $data = Category::find($id);
        foreach ($data->children as $child) {
            $child->parent_id = NULL;
            $child->save();
        }
        $data->delete();
        return redirect()->back()->with('message', 'Successfully deleted');
    }

    public function order(Request $request)
    {
        foreach ($request->order as $key => $item) {
            $menu = Category::find($item['id']);
            $menu->order = $key;
            $menu->parent_id = null;
            $menu->save();
            if (isset($item['children'])) {
                foreach ($item['children'] as $chkey => $child) {
                    $children = Category::find($child['id']);
                    $children->parent_id = $menu['id'];
                    $children->order = $chkey;
                    $children->save();
                }
            }
        }
    }
}
