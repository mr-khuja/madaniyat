<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Image;

class DataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function home()
    {
        return view('admin.pages.home');
    }

    public function settings(Request $request)
    {
        $data = DB::table('settings')->first();
        if ($request->isMethod('post')) {
            $validation = [
                'sitename_ru' => 'string',
                'sitename_en' => 'string',
                'sitename_uz' => 'string',
            ];
            $this->validate($request, $validation);

            $logo = $data->logo;
            $favicon = $data->favicon;
            if ($request->hasFile('logo')) {
                $logo = '/storage/' . $request->file('logo')->store('images', 'public');
            }
            if ($request->hasFile('favicon')) {
                $favicon = '/storage/' . $request->file('favicon')->store('images', 'public');
            }
            DB::table('settings')->where('id', 1)->update([
                'logo' => $logo,
                'favicon' => $favicon,
                'sitename_ru' => $request->sitename_ru,
                'sitename_en' => $request->sitename_en,
                'sitename_uz' => $request->sitename_uz,
            ]);

            return redirect()->back()->with('message', 'Successfully updated');
        }
        return view('admin.pages.settings.settings')->withData($data);
    }

    public function socials(Request $request)
    {
        $data = DB::table('settings')->first();
        if ($request->isMethod('post')) {

            DB::table('settings')->where('id', 1)->update([
                'twitter' => $request->twitter,
                'facebook' => $request->facebook,
                'instagram' => $request->instagram,
                'youtube' => $request->youtube,
                'telegram' => $request->telegram,
            ]);

            return redirect()->back()->with('message', 'Successfully updated');
        }
        return view('admin.pages.settings.socials')->withData($data);
    }

    public function contacts(Request $request)
    {
        $data = DB::table('settings')->first();
        if ($request->isMethod('post')) {
            $validation = [
                'address_uz' => 'string'
            ];
            $this->validate($request, $validation);

            DB::table('settings')->where('id', 1)->update([
                'address_uz' => $request->address_uz,
                'address_en' => $request->address_en,
                'address_ru' => $request->address_ru,
                'transport_uz' => $request->transport_uz,
                'transport_ru' => $request->transport_ru,
                'transport_en' => $request->transport_en,
                'point_uz' => $request->point_uz,
                'point_ru' => $request->point_ru,
                'point_en' => $request->point_en,
                'email' => $request->email,
                'fax' => $request->fax,
                'phone' => $request->phone,
                'mobile' => $request->mobile,
                'map' => $request->map,
            ]);

            return redirect()->back()->with('message', 'Successfully updated');
        }
        return view('admin.pages.settings.contacts')->withData($data);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }


}
