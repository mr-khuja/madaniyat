<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Filial;
use Illuminate\Http\Request;

class FilialController extends Controller
{
    public function list()
    {
        $data = Filial::orderBy('created_at', 'DESC')->get();
        return view('admin.pages.list')->withData($data)->withType('filial')->withTrans(true);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $validation = [
                'title_uz' => 'required|string',
                'title_en' => 'required|string',
                'title_ru' => 'required|string',
                'address_uz' => 'required|string',
                'address_en' => 'required|string',
                'address_ru' => 'required|string',
                'head_uz' => 'required|string',
                'head_en' => 'required|string',
                'head_ru' => 'required|string',
                'city' => 'required|string',
                'phone' => 'required|string',
                'photo' => 'required|string',
                'email' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data = new Filial;
            $data->title_uz = $request->title_uz;
            $data->title_en = $request->title_en;
            $data->title_ru = $request->title_ru;
            $data->address_uz = $request->address_uz;
            $data->address_en = $request->address_en;
            $data->address_ru = $request->address_ru;
            $data->head_uz = $request->head_uz;
            $data->head_en = $request->head_en;
            $data->head_ru = $request->head_ru;
            $data->city = $request->city;
            $data->phone = $request->phone;
            $data->photo = $request->photo;
            $data->email = $request->email;
            $data->save();

            return redirect()->route('filial')->with('message', 'Successfully created');

        }
        $items = [
            'title_uz', 'title_en', 'title_ru', 'address_uz', 'address_en', 'address_ru', 'head_uz', 'head_en', 'head_ru', 'city', 'phone', 'email', 'photo'
        ];
        return view('admin.pages.action')->withType('filial')->withAction('create')->withItems($items);
    }

    public function edit(Request $request, $id)
    {
        $data = Filial::find($id);
        if ($request->isMethod('post')) {
            $validation = [
                'title_uz' => 'required|string',
                'title_en' => 'required|string',
                'title_ru' => 'required|string',
                'address_uz' => 'required|string',
                'address_en' => 'required|string',
                'address_ru' => 'required|string',
                'head_uz' => 'required|string',
                'head_en' => 'required|string',
                'head_ru' => 'required|string',
                'city' => 'required|string',
                'phone' => 'required|string',
                'photo' => 'required|string',
                'email' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data->title_uz = $request->title_uz;
            $data->title_en = $request->title_en;
            $data->title_ru = $request->title_ru;
            $data->address_uz = $request->address_uz;
            $data->address_en = $request->address_en;
            $data->address_ru = $request->address_ru;
            $data->head_uz = $request->head_uz;
            $data->head_en = $request->head_en;
            $data->head_ru = $request->head_ru;
            $data->city = $request->city;
            $data->phone = $request->phone;
            $data->photo = $request->photo;
            $data->email = $request->email;
            $data->save();

            return redirect()->route('filial')->with('message', 'Successfully edited');

        }

        $items = [
            'title_uz', 'title_en', 'title_ru', 'address_uz', 'address_en', 'address_ru', 'head_uz', 'head_en', 'head_ru', 'city', 'phone', 'email', 'photo'
        ];
        return view('admin.pages.action')->withType('filial')->withAction('edit')->withItems($items)->withData($data);
    }

    public function delete($id)
    {
        Filial::destroy($id);
        return redirect()->back()->with('message', 'Successfully deleted');
    }
}
