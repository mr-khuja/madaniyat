<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Link;

class LinkController extends Controller
{
    public function list()
    {
        $data = Link::orderBy('created_at', 'DESC')->get();
        return view('admin.pages.list')->withData($data)->withType('links')->withTrans(true);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $validation = [
                'title_uz' => 'required|string',
                'title_en' => 'required|string',
                'title_ru' => 'required|string',
                'image' => 'required|string',
                'order' => 'required',
                'link' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data = new Link;
            $data->title_uz = $request->title_uz;
            $data->title_en = $request->title_en;
            $data->title_ru = $request->title_ru;
            $data->image = $request->image;
            $data->order = $request->order;
            $data->link = $request->link;
            $data->save();

            return redirect()->route('links')->with('message', 'Successfully created');

        }
        $items = [
            'title_uz', 'title_en', 'title_ru', 'image', 'link', 'order'
        ];
        return view('admin.pages.action')->withType('links')->withAction('create')->withItems($items);
    }

    public function edit(Request $request, $id)
    {
        $data = Link::find($id);
        if ($request->isMethod('post')) {
            $validation = [
                'title_uz' => 'required|string',
                'title_en' => 'required|string',
                'title_ru' => 'required|string',
                'image' => 'required|string',
                'order' => 'required',
                'link' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data->title_uz = $request->title_uz;
            $data->title_en = $request->title_en;
            $data->title_ru = $request->title_ru;
            $data->image = $request->image;
            $data->order = $request->order;
            $data->link = $request->link;
            $data->save();

            return redirect()->route('links')->with('message', 'Successfully edited');

        }

        $items = [
            'title_uz', 'title_en', 'title_ru', 'image', 'link', 'order'
        ];
        return view('admin.pages.action')->withType('links')->withAction('edit')->withItems($items)->withData($data);
    }

    public function delete($id)
    {
        Link::destroy($id);
        return redirect()->back()->with('message', 'Successfully deleted');
    }
}
