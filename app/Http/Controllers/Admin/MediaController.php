<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Video;
use App\Models\Photo;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function list(Request $request)
    {
        $photo = Photo::orderBy('created_at', 'DESC')->get();
        $video = Video::orderBy('created_at', 'DESC')->get();
        return view('admin.pages.gallery')->withVideo($video)->withPhoto($photo);
    }

    public function photo(Request $request)
    {
        if ($request->isMethod('post')) {
            $validation = [
                'image' => 'required|string',
            ];
            $this->validate($request, $validation);

            $photos = explode(',', $request->image);
            foreach ($photos as $photo) {
                $data = new Photo;
                $data->image = $photo;
                $data->save();
            }

            return redirect()->route('media')->with('message', 'Successfully created');

        }
        $items = [
            'image'
        ];
        return view('admin.pages.action')->withType('media')->withContent('photo')->withAction('create')->withItems($items);
    }

    public function video(Request $request)
    {
        if ($request->isMethod('post')) {
            $validation = [
                'poster' => 'required|string',
                'video' => 'required|file|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
            ];
            $this->validate($request, $validation);


            $data = new Video;
            $data->poster = $request->poster;
            $data->video = '/storage/' . $request->video->store('videos', 'public');
            $data->save();

            return redirect()->route('media')->with('message', 'Successfully created');

        }
        $items = [
            'poster', 'video'
        ];
        return view('admin.pages.action')->withType('media')->withContent('video')->withAction('create')->withItems($items);
    }

    public function delete($type, $id)
    {
        if ($type == 'photo') {
            Photo::destroy($id);
        } else {
            Video::destroy($id);
        }
        return redirect()->route('media')->with('message', 'Successfully deleted');
    }
}
