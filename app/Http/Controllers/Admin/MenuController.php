<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function list(Request $request)
    {
        $data = Menu::orderBy('order')->where('parent_id', null)->where('lang', 'ru')->get();
        return view('admin.pages.menu.list')->withMenu($data);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
                'order' => 'required|numeric',
            ];
            $this->validate($request, $validation);

            $data = new Menu;
            $data->title = $request->title;
            $data->short = $request->short;
            $data->order = $request->order;
            $data->link = $request->link;
            $data->lang = 'ru';
            $data->save();

            return redirect()->route('menu')->with('message', 'Successfully created');

        }
        return view('admin.pages.menu.create');
    }

    public function edit(Request $request, $id)
    {
        $data = Menu::find($id);
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
                'order' => 'required|numeric',
            ];
            $this->validate($request, $validation);

            $data->title = $request->title;
            $data->link = $request->link;
            $data->short = $request->short;
            $data->order = $request->order;
            $data->lang = 'ru';
            $data->save();

            return redirect()->route('menu')->with('message', 'Successfully edited');

        }

        return view('admin.pages.menu.edit')->withData($data);
    }

    public function translate(Request $request, $id, $lang)
    {
        $parent = Menu::find($id);
        $data = Menu::where('trans_id', $id)->where('lang', $lang)->first();
        if (!$data) {
            $data = new Menu;
        }
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data->title = $request->title;
            $data->link = $parent->link;
            $data->short = $request->short;
            $data->order = $parent->order;
            $data->trans_id = $id;
            $data->lang = $lang;
            $data->save();

            return redirect()->route('menu')->with('message', 'Successfully translated');

        }

        return view('admin.pages.menu.translate')->withData($data)->withLang($lang)->withId($id);
    }

    public function delete($id)
    {
        $data = Menu::find($id);
        Menu::where('trans_id', $id)->delete();
        foreach ($data->children as $child) {
            $child->parent_id = NULL;
            $child->save();
        }
        $data->delete();
        return redirect()->back()->with('message', 'Successfully deleted');
    }

    public function order(Request $request)
    {
        foreach ($request->order as $key => $item) {
            $menu = Menu::find($item['id']);
            $menu->order = $key;
            $menu->parent_id = null;
            $menu->save();
            if (isset($item['children'])) {
                foreach ($item['children'] as $chkey => $child) {
                    $children = Menu::find($child['id']);
                    $children->parent_id = $menu['id'];
                    $children->order = $chkey;
                    $children->save();
                }
            }
        }
    }
}
