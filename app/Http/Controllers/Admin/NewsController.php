<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
use App\User;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function list()
    {
        $data = News::orderBy('created_at', 'DESC')->where('trans_id', null)->where('type', NULL)->get();
        return view('admin.pages.list')->withData($data)->withType('news');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
//                'image' => 'required|string',
                'short' => 'required|string',
                'body' => 'required|string',
                'category' => 'required|string',
                'user_id' => 'required',
                'created_at' => 'required',
            ];
            $this->validate($request, $validation);

            $data = new News;
            $data->title = $request->title;
            $data->image = $request->image;
            $data->body = $request->body;
            $data->short = $request->short;
            $data->category = $request->category;
            $data->user_id = $request->user_id;
            $data->lang = 'ru';
            $data->created_at = date('Y-m-d H:i:s', strtotime($request->created_at));
            $data->save();

            return redirect()->route('news')->with('message', 'Successfully created');

        }

        $users = User::get();
        $categories = Category::where('parent_id', NULL)->orderBy('order', 'ASC')->get();

        $items = [
            'title', 'image', 'short', 'body', 'lang', 'category', 'created_at', 'user_id'
        ];
        return view('admin.pages.action')
            ->withType('news')
            ->withAction('create')
            ->withItems($items)
            ->withUsers($users)
            ->withCategories($categories);
    }

    public function edit(Request $request, $id)
    {
        $data = News::find($id);
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
                'short' => 'required|string',
                'body' => 'required|string',
//                'image' => 'required|string',
                'user_id' => 'required',
                'category' => 'required|string',
                'created_at' => 'required',
            ];
            $this->validate($request, $validation);

            $data->title = $request->title;
            $data->image = $request->image;
            $data->body = $request->body;
            $data->short = $request->short;
            $data->category = $request->category;
            $data->user_id = $request->user_id;
            $data->created_at = date('Y-m-d H:i:s', strtotime($request->created_at));
            $data->save();

            return redirect()->route('news')->with('message', 'Successfully edited');

        }

        $users = User::get();
        $categories = Category::where('parent_id', NULL)->orderBy('order', 'ASC')->get();

        $items = [
            'title', 'image', 'short', 'body', 'lang', 'category', 'created_at', 'user_id'
        ];
        return view('admin.pages.action')
            ->withType('news')
            ->withAction('edit')
            ->withItems($items)
            ->withUsers($users)
            ->withData($data)
            ->withCategories($categories);
    }

    public function translate(Request $request, $id, $lang)
    {
        $parent = News::find($id);
        $data = News::where('trans_id', $id)->where('lang', $lang)->first();
        if (!$data) {
            $data = new News;
        }
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
                'short' => 'required|string',
                'body' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data->title = $request->title;
            $data->body = $request->body;
            $data->short = $request->short;
            $data->image = $parent->image;
            $data->category = $parent->category;
            $data->user_id = $parent->user_id;
            $data->trans_id = $id;
            $data->lang = $lang;
            $data->created_at = $parent->created_at;
            $data->save();

            return redirect()->route('news')->with('message', 'Successfully translated');

        }

        $items = [
            'title', 'short', 'body', 'lang',
        ];
        return view('admin.pages.action')
            ->withType('news')
            ->withAction('translate')
            ->withItems($items)
            ->withData($data)
            ->withLang($lang)
            ->withId($id);
    }

    public function delete($id)
    {
        $data = News::find($id);
        News::where('trans_id', $id)->delete();
        $data->delete();
        return redirect()->back()->with('message', 'Successfully deleted');
    }
}
