<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\User;
use Illuminate\Http\Request;

class NewzController extends Controller
{
    public function list()
    {
        $data = News::orderBy('created_at', 'DESC')->where('trans_id', null)->where('category', NULL)->get();
        return view('admin.pages.list')->withData($data)->withType('newz');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
                'image' => 'required|string',
                'short' => 'required|string',
                'body' => 'required|string',
                'type' => 'required|string',
                'user_id' => 'required',
                'created_at' => 'required',
            ];
            $this->validate($request, $validation);

            $data = new News;
            $data->title = $request->title;
            $data->image = $request->image;
            $data->body = $request->body;
            $data->short = $request->short;
            $data->type = $request->type;
            $data->user_id = $request->user_id;
            $data->lang = 'ru';
            $data->created_at = date('Y-m-d H:i:s', strtotime($request->created_at));
            $data->save();

            return redirect()->route('newz')->with('message', 'Successfully created');

        }

        $users = User::get();
        $types = [
            'all' => 'Общие',
            'min' => 'Новости министерства',
        ];

        $items = [
            'title', 'image', 'short', 'body', 'lang', 'type', 'created_at', 'user_id'
        ];
        return view('admin.pages.action')
            ->withType('newz')
            ->withAction('create')
            ->withItems($items)
            ->withUsers($users)
            ->withTypes($types);
    }

    public function edit(Request $request, $id)
    {
        $data = News::find($id);
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
                'short' => 'required|string',
                'body' => 'required|string',
                'image' => 'required|string',
                'user_id' => 'required',
                'type' => 'required|string',
                'created_at' => 'required',
            ];
            $this->validate($request, $validation);

            $data->title = $request->title;
            $data->image = $request->image;
            $data->body = $request->body;
            $data->short = $request->short;
            $data->type = $request->type;
            $data->user_id = $request->user_id;
            $data->created_at = date('Y-m-d H:i:s', strtotime($request->created_at));
            $data->save();

            return redirect()->route('newz')->with('message', 'Successfully edited');

        }

        $users = User::get();

        $types = [
            'all' => 'Общие',
            'min' => 'Новости министерства',
        ];

        $items = [
            'title', 'image', 'short', 'body', 'lang', 'type', 'created_at', 'user_id'
        ];
        return view('admin.pages.action')
            ->withType('newz')
            ->withAction('edit')
            ->withItems($items)
            ->withUsers($users)
            ->withData($data)
            ->withTypes($types);
    }

    public function translate(Request $request, $id, $lang)
    {
        $parent = News::find($id);
        $data = News::where('trans_id', $id)->where('lang', $lang)->first();
        if (!$data) {
            $data = new News;
        }
        if ($request->isMethod('post')) {
            $validation = [
                'title' => 'required|string',
                'short' => 'required|string',
                'body' => 'required|string',
            ];
            $this->validate($request, $validation);

            $data->title = $request->title;
            $data->body = $request->body;
            $data->short = $request->short;
            $data->image = $parent->image;
            $data->type = $parent->type;
            $data->user_id = $parent->user_id;
            $data->trans_id = $id;
            $data->lang = $lang;
            $data->created_at = $parent->created_at;
            $data->save();

            return redirect()->route('newz')->with('message', 'Successfully translated');

        }

        $items = [
            'title', 'short', 'body', 'lang',
        ];
        return view('admin.pages.action')
            ->withType('newz')
            ->withAction('translate')
            ->withItems($items)
            ->withData($data)
            ->withLang($lang)
            ->withId($id);
    }

    public function delete($id)
    {
        $data = News::find($id);
        News::where('trans_id', $id)->delete();
        $data->delete();
        return redirect()->back()->with('message', 'Successfully deleted');
    }
}
