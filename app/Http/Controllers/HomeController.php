<?php

namespace App\Http\Controllers;

use App\Models\Baner;
use App\Models\Category;
use App\Models\Link;
use App\Models\Photo;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use App\Models\Menu;
use App\Models\Newz;
use App\Models\News;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function getStatic()
    {

        $lang = app()->getLocale();
        $menu = Menu::where('parent_id', NULL)->orderBy('order', 'ASC')->where('lang', $lang)->get();
        View::share('menu', $menu);
        $baners = Baner::orderBy('order', 'ASC')->where('lang', $lang)->get();
        View::share('baners', $baners);
        $links = Link::orderBy('order', 'ASC')->get();
        View::share('links', $links);
    }

    public function language($locale)
    {
        if (in_array($locale, \Config::get('app.locales'))) {
            Session::put('locale', $locale);
        } else {
            $lang = app()->getLocale();
            Session::put('locale', $lang);
        }
        return redirect()->back();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lang = app()->getLocale();
        $this->getStatic();

        $newz = [];
        $newz[0] = News::where('type', 'all')->where('lang', $lang)->orderBy('created_at', 'DESC')->limit(5)->get();
        $newz[1] = News::where('type', 'min')->where('lang', $lang)->orderBy('created_at', 'DESC')->limit(5)->get();
        $cats = Category::where('parent_id', NULL)->orderBy('order', 'ASC')->with(['news' => function ($q) {
            return $q->limit(4);
        }])->get();

        $photos = Photo::limit(4)->get();
        $videos = Video::limit(8)->get();

        return view('pages.home')->with([
            'news' => $newz,
            'newz' => $cats,
            'photos' => $photos,
            'videos' => $videos,
        ]);
    }

    public function newz()
    {
        $lang = app()->getLocale();
        $this->getStatic();
        $newz[0] = News::where('type', 'all')->where('lang', $lang)->orderBy('created_at', 'DESC')->limit(5)->get();
        $newz[1] = News::where('type', 'min')->where('lang', $lang)->orderBy('created_at', 'DESC')->limit(5)->get();

        return view('pages.newz')->withData($newz);
    }

    public function news($id = 0)
    {
        $this->getStatic();
        $data = Category::where('parent_id', NULL)->orderBy('order', 'ASC')->with('news')->get();
        if ($id == 0) {
            $news = [];
        } else {
            $news = News::where('category', $id)->get();
        }

        return view('pages.news')->withData($data)->withNews($news);
    }

    public function post($id)
    {
        $this->getStatic();
        $news = News::find($id);
        $data = Category::where('parent_id', NULL)->orderBy('order', 'ASC')->with('news')->get();
        return view('pages.post')->withData($data)->withNews($news);
    }


}
