<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $guarded = [];

    public function author()
    {
        return $this->belongsTo('App\User', 'id', 'user_id');
    }
}
