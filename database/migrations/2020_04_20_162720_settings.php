<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('sitename_uz');
            $table->string('sitename_ru');
            $table->string('sitename_en');
            $table->string('phone');
            $table->string('mobile');
            $table->string('fax');
            $table->string('email');
            $table->text('address_uz');
            $table->text('address_ru');
            $table->text('address_en');
            $table->text('transport_uz');
            $table->text('transport_ru');
            $table->text('transport_en');
            $table->text('point_uz');
            $table->text('point_ru');
            $table->text('point_en');
            $table->text('map');
            $table->string('logo');
            $table->string('favicon');
            $table->string('twitter')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('youtube')->nullable();
            $table->string('telegram')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
