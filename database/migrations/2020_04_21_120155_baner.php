<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Baner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baner', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('short');
            $table->string('link');
            $table->string('image');
            $table->string('lang');
            $table->integer('order');
            $table->bigInteger('trans_id')->unsigned()->nullable();
            $table->foreign('trans_id')->references('id')->on('baner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
