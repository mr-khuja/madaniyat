<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Links extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link', function (Blueprint $table) {
            $table->id();
            $table->string('title_uz');
            $table->string('title_ru');
            $table->string('title_en');
            $table->string('image');
            $table->string('link');
            $table->integer('order');
            $table->timestamps();
        });
        Schema::create('photo', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->timestamps();
        });
        Schema::create('video', function (Blueprint $table) {
            $table->id();
            $table->string('poster')->default('/img/post.jpg');
            $table->string('video');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
