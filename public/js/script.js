$(document).ready(function() {
	$(".banner").slick({
		dots: true,
		infinite: true,
		autoplay: false,
		autoplaySpeed: 3000,
		speed: 1000,
		slidesToShow: 1,
		fade: true,
		arrows: true
	});
	if($( window ).width() > 768){
        new WOW().init();
    }
	$("header .section-top .languages-search button").click(function(){
		$(this).toggleClass("active");
		$("header .header-search-field").toggleClass("active");
	});
	$("header .section-top .languages-search li a").click(function(){
		$("header .section-top .languages-search ul li ul").toggleClass("active");
	});
	$("main .section-news .news-menu ul li").click(function(){
		$(this).addClass("active");
		$("main .section-news .news-menu ul li").removeClass("active");
	});
	$(".big-button").click(function(){
		$(this).toggleClass("active");
		$(".buttons-group").toggleClass("active");
	});
	$(".toggle-nav").click(function(){
		$(this).toggleClass("active");
		$("header .section-bottom .menu").toggleClass("active");
        $(".owerlay").toggleClass("active");
        $("body").toggleClass("active");
    });

    $(".owerlay").click(function () {
        $(this).removeClass("active");
        $("header .section-bottom .menu").removeClass("active");
        $(".toggle-nav").removeClass("active");
        $("body").removeClass("active");
    });
    $('.mobile-ver').on('click', function (e) {
        e.preventDefault();
        popupWindow(window.location.href, '', 500, 700);
    })

    $("header .section-bottom .menu ul li.close-icon").click(function () {
        $("header .section-bottom .menu").removeClass("active");
        $(".owerlay").removeClass("active");
        $(".toggle-nav").removeClass("active");
        $("body").removeClass("active");
    });
    $('.changefont').on('click', function () {
        var $action = $(this).attr('data-id');
        $(document).find('a, p, span, h1, h2, h3, h4, h5').each(function () {
            var $fz = $(this).css('font-size');
            var $array = $fz.split('px');
            var $size = parseInt($array[0]);
            switch ($action) {
                case 'plus':
                    $size = $size + 2;
                    $(this).css({'font-size': $size + 'px'})
                    break;
                case 'minus':
                    $size = $size - 2;
                    $(this).css({'font-size': $size + 'px'})
                    break;
                case 'default':
                    $(this).removeAttr('style');
                    break
            }
        })
    })
    $('.changecolor').on('click', function () {

    })

    $('.quaters .showmore').on('click', function(){
        if($(this).parent('div').hasClass('active')){
            $(this).parent('div').removeClass('active')
            $(this).parent('div').find('.text-items').slideUp()

        }else{
            $('.quaters .text').removeClass('active')
            $('.quaters .text-items').slideUp()

            $(this).parent('div').find('.text-items').slideDown()
            $(this).parent('div').addClass('active')
        }
    })
});

function popupWindow(url, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}
