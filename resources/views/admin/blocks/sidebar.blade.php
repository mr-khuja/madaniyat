<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="/admin" class="brand-link">
        <span class="brand-text font-weight-light">Панель управления</span>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">Данные сайта</li>
                <li class="nav-item">
                    <a href="/admin/menu" class="@if(request()->is('admin/menu/*') || request()->is('admin/menu')) active @endif nav-link">
                        <i class="nav-icon fas fa-list"></i>
                        <p>Меню</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/baners" class="@if(request()->is('admin/baners') || request()->is('admin/baners/*')) active @endif nav-link">
                        <i class="nav-icon far fa-images"></i>
                        <p>Банеры</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/newz" class="@if(request()->is('admin/newz') || request()->is('admin/newz/*')) active @endif nav-link">
                        <i class="nav-icon far fa-newspaper"></i>
                        <p>Новости (Мин.)</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/news" class="@if(request()->is('admin/news') || request()->is('admin/news/*')) active @endif nav-link">
                        <i class="nav-icon far fa-newspaper"></i>
                        <p>Новости (по напр.)</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/category" class="@if(request()->is('admin/category') || request()->is('admin/category/*')) active @endif nav-link">
                        <i class="nav-icon fas fa-clipboard-list"></i>
                        <p>Направления</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/links" class="@if(request()->is('admin/links') || request()->is('admin/links/*')) active @endif nav-link">
                        <i class="nav-icon fas fa-link"></i>
                        <p>Полезные ссылки</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/media" class="@if(request()->is('admin/media') || request()->is('admin/media/*')) active @endif nav-link">
                        <i class="nav-icon fas fa-photo-video"></i>
                        <p>Медиа</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/admin/filial" class="@if(request()->is('admin/filial') || request()->is('admin/filial/*')) active @endif nav-link">
                        <i class="nav-icon fas fa-photo-video"></i>
                        <p>Рег. управления</p>
                    </a>
                </li>
                <li class="@if(request()->is('admin/settings') || request()->is('admin/contacts') || request()->is('admin/socials')) menu-open @endif nav-item has-treeview">
                    <a href="#" class="@if(request()->is('admin/settings') || request()->is('admin/contacts') || request()->is('admin/socials')) active @endif nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>Настройки<i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/admin/settings" class="@if(request()->is('admin/settings')) active @endif nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Настройки сайта</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/contacts" class="@if(request()->is('admin/contacts')) active @endif nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Контактные данные</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/socials" class="@if(request()->is('admin/socials')) active @endif nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Социальные сети</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
