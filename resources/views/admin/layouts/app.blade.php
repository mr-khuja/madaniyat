<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Панель управления | {{$settings->sitename_ru}}</title>
    <link rel="shortcut icon" href="{{$settings->favicon}}" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/admin-side/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/admin-side/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <link rel="stylesheet" href="/admin-side/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="/admin-side/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="/admin-side/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <link rel="stylesheet" href="/admin-side/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="/admin-side/plugins/ekko-lightbox/ekko-lightbox.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
    @stack('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    @include('admin.blocks.header')

    @include('admin.blocks.sidebar')

    <div class="content-wrapper">
        <div class="content-header">
        </div>
        <section class="content">
            <div class="container-fluid">
                @yield('content')
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>&copy; 2005-{{date('Y')}} <a href="https://datasite.uz">DataSite Technology</a>.</strong>
        Все права защищены.
        <div class="float-right d-none d-sm-inline-block">
            <b>Версия</b> 0.4
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/admin-side/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/admin-side/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/admin-side/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/admin-side/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/admin-side/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/admin-side/plugins/datatables/jquery.dataTables.js"></script>
<script src="/admin-side/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="/admin-side/plugins/toastr/toastr.min.js"></script>
<script src="/admin-side/plugins/moment/moment.min.js"></script>
<script src="/admin-side/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<!-- AdminLTE App -->
<script src="/admin-side/dist/js/adminlte.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@stack('js')
@if(session('message'))
    <script type="text/javascript">
        $(function () {
            toastr.success('{{session("message")}}')
        });

    </script>
@endif
@foreach($errors->all() as $error)
    <script type="text/javascript">
        $(function () {
            toastr.error('{{$error}}')
        });

    </script>
@endforeach
<script>
    $(function () {
        $(document).on('click', '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox({
                alwaysShowClose: true
            });
        });

    });
</script>
</body>
</html>
