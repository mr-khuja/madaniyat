@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                @php $arr = ['create' => 'Создание', 'edit' => 'Изменение', 'translate' => 'Перевод'] @endphp
                <h3 class="card-title">{{$arr[$action]}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form"
                  method="post"
                  enctype="multipart/form-data"
                  @if($type != 'media')
                  action="/admin/{{$type}}/{{$action}}@php
                      if($action == 'translate'){ print('/'.$id); }
                      if($action == 'edit'){ print('/'.$data->id); }
                      if($action == 'translate'){ print('/'.$lang); }
                  @endphp"
                  @else
                    action="/admin/media/{{$content}}"
                    @endif>
                @csrf
                <div class="card-body">
                    @foreach($items as $item)
                        @include('admin.templates.'.$item)
                    @endforeach
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
@endsection
