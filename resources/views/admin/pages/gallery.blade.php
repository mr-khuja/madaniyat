@extends('admin.layouts.app')

@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">Медиа</h3>
                <ul class="nav nav-pills ml-auto p-2">
                    <li class="nav-item" style="margin-right: 10px;"><a class="btn btn-success"
                                                                        href="/admin/media/photo">Создать фото</a></li>
                    <li class="nav-item"><a class="btn btn-success" href="/admin/media/video">Создать видео</a></li>
                </ul>
            </div>
            <div class="card-body">
                <div>
                    <div class="btn-group w-100 mb-2">
                        <a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> Все </a>
                        <a class="btn btn-info" href="javascript:void(0)" data-filter="1">Фото</a>
                        <a class="btn btn-info" href="javascript:void(0)" data-filter="2">Видео</a>
                    </div>
                    <div class="mb-2">
                        <a class="btn btn-secondary" href="javascript:void(0)" data-shuffle>Разбросать</a>
                        <div class="float-right">
                            <select class="custom-select" style="width: auto;" data-sortOrder>
                                <option value="index">Сортировать по ID</option>
                                <option value="sortData">Сортировать по дате</option>
                            </select>
                            <div class="btn-group">
                                <a class="btn btn-default" href="javascript:void(0)" data-sortAsc> По возрастанию </a>
                                <a class="btn btn-default" href="javascript:void(0)" data-sortDesc> По убыванию </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="filter-container p-0 row">
                        @foreach($photo as $item)
                            <div class="filtr-item col-sm-2" data-category="1" data-sort="{{$item->created_at}}">
                                <a href="{{$item->image}}" style="display: block;text-align: center"
                                   data-toggle="lightbox">
                                    <img style="max-height: 70px;min-height: 70px;" src="{{$item->image}}"
                                         class="img-fluid mb-2"/>
                                </a>
                                <a style="display: block" href="/admin/media/delete/photo/{{$item->id}}" class="btn btn-danger"><i
                                        class="far fa-trash-alt"></i></a>
                            </div>
                        @endforeach
                        @foreach($video as $item)
                            <div class="filtr-item col-sm-2" data-category="2" data-sort="{{$item->created_at}}">
                                <a data-fancybox="video" href="{{$item->video}}">
                                    <img style="max-height: 70px;min-height: 70px;" src="{{$item->poster}}"
                                         class="img-fluid mb-2"/>
                                </a>
                                <a style="display: block" href="/admin/media/delete/video/{{$item->id}}" class="btn btn-danger"><i
                                        class="far fa-trash-alt"></i></a>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
@push('css')
@endpush
@push('js')
    <script src="/admin-side/plugins/filterizr/jquery.filterizr.min.js"></script>
    <script>
        $(function () {
            $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });

            $('.filter-container').filterizr({gutterPixels: 3});
            $('.btn[data-filter]').on('click', function () {
                $('.btn[data-filter]').removeClass('active');
                $(this).addClass('active');
            });
        })
    </script>
@endpush
