@extends('admin.layouts.app')

@section('content')
    <div class="col-12">
        <!-- /.card -->

        <div class="card">
            <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">Список</h3>
                <ul class="nav nav-pills ml-auto p-2">
                    <li class="nav-item"><a class="btn btn-success" href="/admin/{{$type}}/create">Создать</a></li>
                </ul>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Создано</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $item)
                        <tr>
                            <td style="vertical-align: middle">{{$item->id}}</td>
                            @if(isset($item->title_ru))
                                <td style="vertical-align: middle">{{$item->title_ru}}</td>
                            @else
                                <td style="vertical-align: middle">{{$item->title}}</td>
                            @endif
                            <td style="vertical-align: middle">{{date('d.m.Y', strtotime($item->created_at))}}</td>
                            <td style="vertical-align: middle">
                                <div class="btn-group">
                                    <a href="/admin/{{$type}}/edit/{{$item->id}}" class="btn btn-info">Изменить</a>
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item" href="/admin/{{$type}}/edit/{{$item->id}}">Изменить</a>
                                        @if(!isset($trans))
                                            <a class="dropdown-item"
                                               href="/admin/{{$type}}/translate/{{$item->id}}/uz">Перевод UZ</a>
                                            <a class="dropdown-item"
                                               href="/admin/{{$type}}/translate/{{$item->id}}/en">Перевод EN</a>
                                            <div class="dropdown-divider"></div>
                                        @endif
                                        <a class="dropdown-item"
                                           href="/admin/{{$type}}/delete/{{$item->id}}">Удалить</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
@endsection
@push('js')
    <script>
        $(function () {
            $('#example').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json"
                },
                'order': [[0, 'desc']]
            });
        });
    </script>
@endpush
