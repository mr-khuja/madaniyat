@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Создать новое меню</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="/admin/menu/create">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Название</label>
                        <input type="text" class="form-control" name="title" id="title">
                    </div>
                    <div class="form-group">
                        <label for="short">Подзаголовок (не обязательно)</label>
                        <input type="text" class="form-control" name="short" id="short">
                    </div>
                    <div class="form-group">
                        <label for="link">Ссылка </label>
                        <input type="text" class="form-control" name="link" id="link">
                    </div>
                    <div class="form-group">
                        <label>Язык</label>
                        <select name="lang" disabled class="form-control">
                            <option value="ru" selected>Русский</option>
                            <option value="en">English</option>
                            <option value="uz">O'zbek</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="order">Очередь</label>
                        <input value="0" type="number" class="form-control" name="order" id="order">
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
@endsection
