@extends('admin.layouts.app')

@section('content')
    <div class="col-12">
        <!-- /.card -->

        <div class="card">
            <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">Список меню</h3>
                <ul class="nav nav-pills ml-auto p-2">
                    <li class="nav-item"><a class="btn btn-success" href="/admin/menu/create">Создать</a></li>
                </ul>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="dd">
                    <ol class="dd-list">
                        @foreach($menu as $item)
                            <li class="dd-item dd3-item" data-id="{{$item->id}}">
                                <div class="dd3-handle dd-handle">
                                </div>
                                <div class="dd3-content">{{$item->title}}</div>
                                <div class="btn-group">
                                    <a href="/admin/menu/edit/{{$item->id}}" class="btn btn-info">Изменить</a>
                                    <button type="button" class="btn btn-info dropdown-toggle"
                                            data-toggle="dropdown">
                                        <spans class="caret"></spans>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a class="dropdown-item"
                                               href="/admin/menu/edit/{{$item->id}}">Изменить</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="/admin/menu/translate/{{$item->id}}/uz">Перевод UZ</a>
                                        </li>
                                        <li><a class="dropdown-item"
                                               href="/admin/menu/translate/{{$item->id}}/en">Перевод EN</a>
                                        </li>
                                        <li class="dropdown-divider"></li>
                                        <li><a class="dropdown-item"
                                               href="/admin/menu/delete/{{$item->id}}">Удалить</a></li>
                                    </ul>
                                </div>
                                @if(count($item->children) > 0)
                                    <ol class="dd-list">
                                        @foreach($item->children as $child)
                                            <li class="dd-item dd3-item" data-id="{{$child->id}}">
                                                <div class="dd3-handle dd-handle">
                                                </div>
                                                <div class="dd3-content">{{$child->title}}</div>
                                                <div class="btn-group">
                                                    <a href="/admin/menu/edit/{{$child->id}}" class="btn btn-info">Изменить</a>
                                                    <button type="button" class="btn btn-info dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        <spans class="caret"></spans>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a class="dropdown-item"
                                                               href="/admin/menu/edit/{{$child->id}}">Изменить</a>
                                                        </li>
                                                        <li><a class="dropdown-item"
                                                               href="/admin/menu/translate/{{$child->id}}/uz">Перевод
                                                                UZ</a>
                                                        </li>
                                                        <li><a class="dropdown-item"
                                                               href="/admin/menu/translate/{{$child->id}}/en">Перевод
                                                                EN</a>
                                                        </li>
                                                        <li class="dropdown-divider"></li>
                                                        <li><a class="dropdown-item"
                                                               href="/admin/menu/delete/{{$child->id}}">Удалить</a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ol>
                                @endif
                            </li>
                        @endforeach
                    </ol>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary saveorder">Сохранить</button>
            </div>
        </div>
        <!-- /.card -->
    </div>
@endsection
@push('css')
    <link rel="stylesheet" href="/css/nestable.css">
    <style>
        .dd .btn {
            padding: .300rem .75rem;
        }
    </style>
@endpush
@push('js')
    <script src="/js/nestable.js"></script>
    <script>
        $(function () {
            $('.dd').nestable({
                maxDepth: 2,
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });

            $('.saveorder').on('click', function () {
                var order = $('.dd').nestable('serialize');
                $.ajax({
                    type: 'POST',
                    url: '/admin/menu/order',
                    data: {order},
                    success: function () {
                        location.reload();
                    }
                });
            });
        })
    </script>
@endpush

