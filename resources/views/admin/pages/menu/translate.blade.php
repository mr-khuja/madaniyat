@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Перевод меню</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="/admin/menu/translate/{{$id}}/{{$lang}}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Название</label>
                        <input value="{{$data->title}}" type="text" class="form-control" name="title" id="title">
                    </div>
                    <div class="form-group">
                        <label for="short">Подзаголовок (не обязательно)</label>
                        <input value="{{$data->short}}" type="text" class="form-control" name="short" id="short">
                    </div>
                    <div class="form-group">
                        <label>Язык</label>
                        <select name="lang" disabled class="form-control">
                            <option value="ru">Русский</option>
                            <option value="en" @if($lang == 'en') selected @endif>English</option>
                            <option value="uz" @if($lang == 'uz') selected @endif>O'zbek</option>
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
@endsection
