@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Контактные данные</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="/admin/contacts">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" value="{{$data->email}}" class="form-control" name="email"
                               id="email">
                    </div>
                    <div class="form-group">
                        <label for="fax">Факс</label>
                        <input type="text" value="{{$data->fax}}" class="form-control" name="fax"
                               id="fax">
                    </div>
                    <div class="form-group">
                        <label for="phone">Телефон</label>
                        <input type="text" value="{{$data->phone}}" class="form-control" name="phone"
                               id="phone">
                    </div>
                    <div class="form-group">
                        <label for="mobile">Телефон доверия</label>
                        <input type="text" value="{{$data->mobile}}" class="form-control" name="mobile"
                               id="mobile">
                    </div>
                    <div class="form-group">
                        <label for="address_uz">Адрес (UZ)</label>
                        <textarea name="address_uz" id="address_uz" class="form-control" rows="5">{{$data->address_uz}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="address_en">Адрес (EN)</label>
                        <textarea name="address_en" id="address_en" class="form-control" rows="5">{{$data->address_en}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="address_ru">Адрес (РУ)</label>
                        <textarea name="address_ru" id="address_ru" class="form-control" rows="5">{{$data->address_ru}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="transport_uz">Транспорт (UZ)</label>
                        <textarea name="transport_uz" id="transport_uz" class="form-control" rows="5">{{$data->transport_uz}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="transport_ru">Транспорт (РУ)</label>
                        <textarea name="transport_ru" id="transport_ru" class="form-control" rows="5">{{$data->transport_ru}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="transport_en">Транспорт (EN)</label>
                        <textarea name="transport_en" id="transport_en" class="form-control" rows="5">{{$data->transport_en}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="point_uz">Ориентир (UZ)</label>
                        <textarea name="point_uz" id="point_uz" class="form-control" rows="5">{{$data->point_uz}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="point_ru">Ориентир (РУ)</label>
                        <textarea name="point_ru" id="point_ru" class="form-control" rows="5">{{$data->point_ru}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="point_en">Ориентир (EN)</label>
                        <textarea name="point_en" id="point_en" class="form-control" rows="5">{{$data->point_en}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="map">Карта</label>
                        <textarea name="map" id="map" class="form-control" rows="5">{{$data->map}}</textarea>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
@endsection
