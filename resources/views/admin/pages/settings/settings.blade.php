@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Настройки сайта</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="/admin/settings">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="sitename_uz">Название сайта (UZ)</label>
                        <input type="text" value="{{$data->sitename_uz}}" class="form-control" name="sitename_uz"
                               id="sitename_uz" placeholder="Введите название сайта (UZ)">
                    </div>
                    <div class="form-group">
                        <label for="sitename_ru">Название сайта (РУ)</label>
                        <input type="text" value="{{$data->sitename_ru}}" class="form-control" name="sitename_ru"
                               id="sitename_ru" placeholder="Введите название сайта (РУ)">
                    </div>
                    <div class="form-group">
                        <label for="sitename_en">Название сайта (EN)</label>
                        <input type="text" value="{{$data->sitename_en}}" class="form-control" name="sitename_en"
                               id="sitename_en" placeholder="Введите название сайта (EN)">
                    </div>
                    <div class="form-group">
                        <label for="logo">Логотип</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input name="logo" type="file" class="custom-file-input" id="logo">
                                <label class="custom-file-label" for="logo">Выбрать файл</label>
                            </div>
                        </div>
                        @isset($data->logo)
                            <img class="mt-3 img-fluid pad" src="{{$data->logo}}" alt="Логотип">
                        @endisset
                    </div>
                    <div class="form-group">
                        <label for="favicon">Иконка</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input name="favicon" type="file" class="custom-file-input" id="favicon">
                                <label class="custom-file-label" for="favicon">Выбрать файл</label>
                            </div>
                        </div>
                        @isset($data->favicon)
                            <img class="mt-3 img-fluid pad" src="{{$data->favicon}}" alt="Иконка">
                        @endisset
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
@endsection
