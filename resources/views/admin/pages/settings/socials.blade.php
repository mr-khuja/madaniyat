@extends('admin.layouts.app')

@section('content')
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Социальные сети</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="/admin/socials">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="twitter">Twitter</label>
                        <input type="text" value="{{$data->twitter}}" class="form-control" name="twitter"
                               id="twitter">
                    </div>
                    <div class="form-group">
                        <label for="facebook">Facebook</label>
                        <input type="text" value="{{$data->facebook}}" class="form-control" name="facebook"
                               id="facebook">
                    </div>
                    <div class="form-group">
                        <label for="instagram">Instagram</label>
                        <input type="text" value="{{$data->instagram}}" class="form-control" name="instagram"
                               id="instagram">
                    </div>
                    <div class="form-group">
                        <label for="youtube">Youtube</label>
                        <input type="text" value="{{$data->youtube}}" class="form-control" name="youtube"
                               id="youtube">
                    </div>
                    <div class="form-group">
                        <label for="telegram">Telegram</label>
                        <input type="text" value="{{$data->telegram}}" class="form-control" name="telegram"
                               id="telegram">
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
@endsection
