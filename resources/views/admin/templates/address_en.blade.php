<div class="form-group">
    <label for="address_en">Адрес (EN)</label>
    <input @if($action == 'create') value="{{old('address_en')}}" @else value="{{$data->address_en}}" @endif
    type="text" class="form-control" name="address_en" id="address_en">
</div>
