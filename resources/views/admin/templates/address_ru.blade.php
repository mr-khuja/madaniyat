<div class="form-group">
    <label for="address_ru">Адрес (РУ)</label>
    <input @if($action == 'create') value="{{old('address_ru')}}" @else value="{{$data->address_ru}}" @endif
    type="text" class="form-control" name="address_ru" id="address_ru">
</div>
