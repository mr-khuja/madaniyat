<div class="form-group">
    <label for="address_uz">Адрес (UZ)</label>
    <input @if($action == 'create') value="{{old('address_uz')}}" @else value="{{$data->address_uz}}" @endif
    type="text" class="form-control" name="address_uz" id="address_uz">
</div>
