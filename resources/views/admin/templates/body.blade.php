<div class="form-group">
    <label for="body">Название</label>
    <textarea name="body" id="my-editor">@if($action == 'create'){!! old('body') !!}@else{!! $data->body !!}@endif</textarea>
</div>
@push('js')
    <script src="https://cdn.ckeditor.com/4.6.2/full-all/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/filemanager?type=image',
            filebrowserImageUploadUrl: '/filemanager/upload?type=image&_token=',
            filebrowserBrowseUrl: '/filemanager?type=file',
            filebrowserUploadUrl: '/filemanager/upload?type=file&_token='
        };
        CKEDITOR.replace('my-editor', options);
    </script>
@endpush
