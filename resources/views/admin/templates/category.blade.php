<div class="form-group">
    <label>Направление</label>
    <select name="category" class="form-control">
        @foreach($categories as $item)
            <option @if($action == 'edit' && $data->category == $item->id) selected @endif value="{{$item->id}}">{{$item->title_ru}}</option>
            @if(count($item->children) > 0)
            @foreach($item->children as $child)
                <option @if($action == 'edit' && $data->category == $child->id) selected @endif value="{{$child->id}}">--{{$child->title_ru}}</option>
                @endforeach
            @endif
        @endforeach
    </select>
</div>
