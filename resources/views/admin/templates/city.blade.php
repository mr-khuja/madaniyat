@php
    $cities = [
       "kara" => "Республика Каракалпакстан",
       "and" => "Андижанская область",
       "bukh" => "Бухарская область",
       "jizz" => "Джизакская область",
       "kashk" => "Кашкадарьинская область",
       "nav" => "Навоинская область",
       "nam" => "Наманганская область",
       "sam" => "Самаркандская область",
       "sur" => "Сурхандарьинская область",
       "sir" => "Сырдарьинская область",
       "tash" => "Ташкентская область",
       "fer" => "Ферганская область",
       "khor" => "Хорезмская область",
       "city" => "Город Ташкент"
   ]
@endphp
<div class="form-group">
    <label>Область/Город</label>
    <select name="city" class="form-control">
        @foreach($cities as $key => $item)
            <option @if($action == 'edit' && $data->city == $item) selected
                    @endif value="{{$key}}">{{$item}}</option>
        @endforeach
    </select>
</div>
