<div class="form-group">
    <label for="created_at">Дата публикации</label>
    <input @if($action == 'create') value="{{date('d.m.Y')}}" @else value="{{$data->created_at->format('d.m.Y')}}"
           @endif type="text" data-position="top left" class="form-control datepicker-here" name="created_at"
           id="created_at" placeholder="Введите название">
</div>
@push('css')
    <link href="/admin-side/plugins/datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
@endpush
@push('js')
    <script src="/admin-side/plugins/datepicker/js/datepicker.min.js"></script>
@endpush
