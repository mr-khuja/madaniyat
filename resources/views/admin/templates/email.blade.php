<div class="form-group">
    <label for="email">E-mail</label>
    <input @if($action == 'create') value="{{old('email')}}" @else value="{{$data->email}}" @endif type="email"
           class="form-control" name="email" id="email">
</div>
