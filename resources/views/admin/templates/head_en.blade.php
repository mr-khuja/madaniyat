<div class="form-group">
    <label for="head_en">Ф.И.О. (EN)</label>
    <input @if($action == 'create') value="{{old('head_en')}}" @else value="{{$data->head_en}}" @endif type="text"
           class="form-control" name="head_en" id="head_en">
</div>
