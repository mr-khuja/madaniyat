<div class="form-group">
    <label for="head_ru">Ф.И.О. (РУ)</label>
    <input @if($action == 'create') value="{{old('head_ru')}}" @else value="{{$data->head_ru}}" @endif type="text"
           class="form-control" name="head_ru" id="head_ru">
</div>
