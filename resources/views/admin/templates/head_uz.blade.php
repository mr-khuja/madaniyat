<div class="form-group">
    <label for="head_uz">Ф.И.О. (UZ)</label>
    <input @if($action == 'create') value="{{old('head_uz')}}" @else value="{{$data->head_uz}}" @endif type="text"
           class="form-control" name="head_uz" id="head_uz">
</div>
