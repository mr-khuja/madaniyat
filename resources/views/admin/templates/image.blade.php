<div class="form-group" style="clear:both">
    <label for="image">Изображение</label>
    <div class="input-group" style="clear:both">
       <span class="input-group-btn">
         <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
           <i class="fa fa-picture-o"></i> Выбрать
         </a>
       </span>
        <input id="thumbnail" @if($action == 'edit') value="{{$data->image}}" @endif class="form-control" type="text" name="image">
    </div>
    <div id="holder" style="margin-top:15px;max-height:100px; clear:both">
        @if($action == 'edit')
            <img height="100px" src="{{$data->image}}" alt="{{$data->title}}"/>
        @endif
    </div>


    @push('js')
        <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
        <script>
            $(function () {
                var lfm = function (id, type, options) {
                    let button = document.getElementById(id);

                    button.addEventListener('click', function () {
                        var route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
                        var target_input = document.getElementById(button.getAttribute('data-input'));
                        var target_preview = document.getElementById(button.getAttribute('data-preview'));

                        window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
                        window.SetUrl = function (items) {
                            var file_path = items.map(function (item) {
                                return item.url;
                            }).join(',');

                            // set the value of the desired input to image url
                            target_input.value = file_path;
                            target_input.dispatchEvent(new Event('change'));

                            // clear previous preview
                            target_preview.innerHtml = '';

                            // set or change the preview image src
                            items.forEach(function (item) {
                                let img = document.createElement('img')
                                img.setAttribute('style', 'height: 5rem')
                                img.setAttribute('src', item.thumb_url)
                                target_preview.appendChild(img);
                            });

                            // trigger change event
                            target_preview.dispatchEvent(new Event('change'));
                        };
                    });
                };
                $('#lfm').filemanager('image');
            });
        </script>
@endpush
