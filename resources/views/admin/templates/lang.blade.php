<div class="form-group">
    <label>Язык</label>
    <select name="lang" disabled class="form-control">
        <option value="ru" @if($action == 'create' || $action == 'edit') selected @endif>Русский</option>
        <option value="en" @if($action == 'translate' && $lang == 'en') selected @endif>English</option>
        <option value="uz" @if($action == 'translate' && $lang == 'uz') selected @endif>O'zbek</option>
    </select>
</div>
