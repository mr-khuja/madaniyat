<div class="form-group">
    <label for="link">Ссылка </label>
    <input @if($action == 'create') value="{{old('link')}}" @else value="{{$data->link}}" @endif type="text" class="form-control" name="link" id="link">
</div>
