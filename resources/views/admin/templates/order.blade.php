<div class="form-group">
    <label for="order">Очередь</label>
    <input @if($action != 'create') value="{{$data->order}}" @else value="0" @endif type="number" class="form-control" name="order" id="order">
</div>
