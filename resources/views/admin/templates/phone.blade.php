<div class="form-group">
    <label for="phone">Номер телефона</label>
    <input @if($action == 'create') value="{{old('phone')}}" @else value="{{$data->phone}}" @endif type="text"
           class="form-control" name="phone" id="phone">
</div>
