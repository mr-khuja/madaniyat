<div class="form-group">
    <label for="short">Подзаголовок</label>
    <input @if($action == 'create') value="{{old('short')}}" @else value="{{$data->short}}" @endif type="text" class="form-control" name="short" id="short">
</div>
