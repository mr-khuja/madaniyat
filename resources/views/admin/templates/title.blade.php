<div class="form-group">
    <label for="title">Название</label>
    <input @if($action == 'create') value="{{old('title')}}" @else value="{{$data->title}}" @endif type="text" class="form-control" name="title" id="title">
</div>
