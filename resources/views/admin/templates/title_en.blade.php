<div class="form-group">
    <label for="title_en">Название (EN)</label>
    <input @if($action == 'create') value="{{old('title_en')}}" @else value="{{$data->title_en}}" @endif type="text" class="form-control" name="title_en" id="title_en">
</div>
