<div class="form-group">
    <label for="title_ru">Название (РУ)</label>
    <input @if($action == 'create') value="{{old('title_ru')}}" @else value="{{$data->title_ru}}" @endif type="text" class="form-control" name="title_ru" id="title_ru">
</div>
