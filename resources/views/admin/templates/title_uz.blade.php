<div class="form-group">
    <label for="title_uz">Название (UZ)</label>
    <input @if($action == 'create') value="{{old('title_uz')}}" @else value="{{$data->title_uz}}" @endif type="text" class="form-control" name="title_uz" id="title_uz">
</div>
