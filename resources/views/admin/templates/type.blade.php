<div class="form-group">
    <label>Тип</label>
    <select name="type" class="form-control">
        @foreach($types as $key => $item)
            <option @if($action == 'edit' && $data->type == $key) selected @endif value="{{$key}}">{{$item}}</option>
        @endforeach
    </select>
</div>
