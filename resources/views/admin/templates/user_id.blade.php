<div class="form-group">
    <label>Автор</label>
    <select name="user_id" class="form-control">
        @foreach($users as $item)
            <option @if($action == 'edit' && $data->user_id == $item->id) selected @endif value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select>
</div>
