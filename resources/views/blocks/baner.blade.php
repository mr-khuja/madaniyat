
<section class="section-banner">
    <div class="banner">
        @foreach($baners as $item)
            <div class="item">
                <img src="{{$item->image}}">
                <div class="item-position">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="banner-heading">
                                    <h1>{{$item->title}}</h1>
                                    <p>{{$item->short}}</p>
                                    <div class="more-details">
                                        <a href="{{$item->link}}">@lang('Подробнее')</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>
