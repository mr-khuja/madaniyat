<section class="section-category">
    <div class="container">
        <div class="row">
            @foreach($links as $item)
                <div class="col-lg-2 col-sm-4 col-6 mobile-block wow fadeInUp">
                    <a href="{{$item->link}}" class="item">
                        <img width="70" src="{{$item->image}}">
                        <p>{{$item['title_'.$l]}}</p>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>
