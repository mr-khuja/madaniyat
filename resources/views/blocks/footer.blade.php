<footer>
    <section class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="menu-footer">
                        <ul>
                            <span>{{$menu[0]->title}}</span>
                            @foreach($menu[0]->children as $child)
                                <li>
                                    <a href="{{$child->link}}">{{$child->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                    <div class="menu-footer">
                        <ul>
                            <span>{{$menu[1]->title}}</span>
                            @foreach($menu[1]->children as $child)
                                <li>
                                    <a href="{{$child->link}}">{{$child->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 mobile-footer-menu">
                    <div class="menu-footer">
                        <ul>
                            <span>Региональные управления</span>
                            <li>
                                <a href="#">Республика Каракалпакстан</a>
                            </li>
                            <li>
                                <a href="#">Андижанская область</a>
                            </li>
                            <li>
                                <a href="#">Бухарская область</a>
                            </li>
                            <li>
                                <a href="#">Джизакская область</a>
                            </li>
                            <li>
                                <a href="#">Кашкадарьинская область</a>
                            </li>
                            <li>
                                <a href="#">Навоинская область</a>
                            </li>
                            <li>
                                <a href="#">Наманганская область</a>
                            </li>
                            <li>
                                <a href="#">Самаркандская область</a>
                            </li>
                            <li>
                                <a href="#">Сурхандарьинская область</a>
                            </li>
                            <li>
                                <a href="#">Сырдарьинская область</a>
                            </li>
                            <li>
                                <a href="#">Ташкентская область</a>
                            </li>
                            <li>
                                <a href="#">Ферганская область</a>
                            </li>
                            <li>
                                <a href="#">Хорезмская область</a>
                            </li>
                            <li>
                                <a href="#">Город Ташкент</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-6 mobile-footer-menu">
                    <div class="menu-footer">
                        <ul>
                            <span>@lang('Контакты')</span>
                            <p><img src="/img/location.png">{{$setting['address_'.$l]}}</p>
                            <p>
                                <img src="/img/phone-footer.png"><a
                                    href="tel:{{$settings->mobile}}">@lang('Единый контакт-центр')
                                    <span>{{$settings->mobile}}</span></a>
                            </p>
                            <a href="#" class="gmail-icon"><img src="/img/gmail.png">{{$settings->email}}</a>
                        </ul>
                        <div class="counter">
                            <span>Счетчик посещаемости</span>
                            <p>Онлайн ............... 40</p>
                            <p>Хосты сегодня .... 772</p>
                            <p>Хосты всего ..... 1146612</p>
                            <span class="weather">Погода</span>
                            <img src="img/weather.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="footer-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                    <div class="text-footer-center">
                        <p>2005-{{date('Y')}} © {{$setting['sitename_'.$l]}}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                    <div class="footer-social-network">
                        <ul>
                            <li>
                                <a href="{{$settings->facebook}}"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li>
                                <a href="{{$settings->twitter}}"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="{{$settings->telegram}}"><i class="fab fa-telegram-plane"></i></a>
                            </li>
                            <li>
                                <a href="{{$settings->instagram}}"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="{{$settings->youtube}}"><i class="fab fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-12 width-footer-left">
                    <div class="text-footer-center">
                        <p>@lang('Обнаружив в тексте ошибку, выделите её и нажмите')</p>
                        <img src="/img/mistake.png">
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-6 col-12 width-footer-right">
                    <div class="data-logo">
                        <p>@lang('Сайт разработан дизайн-студией')</p>
                        <a href="https://datasite.uz"><img src="/img/logo-datasite.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p>@lang('При полном или частичном использовании и цитировании материалов, опубликованных на данном сайте, ссылка на официальный сайт Министерства культуры Республики Узбекистан')</p>
                </div>
            </div>
        </div>
    </section>
</footer>
