<header>
    <section class="section-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-7 col-6">
                    <div class="header-left">
                        <div class="phone">
                            <a href="tel:{{$settings->phone}}"><img src="/img/phone.png">@lang('Тел')
                                : {{$settings->phone}}</a>
                        </div>
                        <div class="social-network">
                            <ul>
                                <li>
                                    <a href="{{$settings->facebook}}"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a href="{{$settings->twitter}}"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="{{$settings->telegram}}"><i class="fab fa-telegram-plane"></i></a>
                                </li>
                                <li>
                                    <a href="{{$settings->instagram}}"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="{{$settings->youtube}}"><i class="fab fa-youtube"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-5 col-6">
                    <div class="header-right">
                        <div class="input">
                            <a href="/login"><img src="/img/user.png">@lang('Вход')</a>
                        </div>
                        <div class="languages-search">
                            @php($langs = ['ru' => 'Рус', 'uz' => 'Uzb', 'en' => 'Eng',])
                            <ul>
                                <li>
                                    <a href="#">{{$langs[app()->getLocale()]}}</a>
                                    <ul>
                                        @foreach($langs as $key => $item)
                                            @if($key != app()->getLocale())
                                                <li>
                                                    <a href="/setlocale/{{$key}}">{{$langs[$key]}}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                            <button>
                                <img src="/img/search.png" class="search">
                                <i class="fas fa-times close-header"></i>
                            </button>
                            <div class="header-search-field">
                                <form>
                                    <input type="text" class="search-field" placeholder="Search …" value="" name="s">
                                    <input class="search-button" type="submit" value="Search">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-5 col-7">
                    <div class="logo">
                        <a href="#">
                            <img src="/img/logo.png">
                            <div class="logo-text">
                                <h1>{{$setting['sitename_'.$l]}}</h1>
                                <p>@lang('официальный сайт')</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-7 col-5">
                    <div class="owerlay"></div>
                    <button class="toggle-nav">
                        <span></span>
                        <span></span>
                    </button>
                    <div class="menu" id="accordion">
                        <ul class="web-site">
                            @foreach($menu as $item)
                                <li>
                                    <a href="{{$item->link}}" class="menu-a">{{$item->title}}
                                        <span>{{$item->subtitle}}</span></a>
                                    @if(count($item->children) >0)
                                        <ul>
                                            @foreach($item->children as $child)
                                                <li>
                                                    <a href="{{$child->link}}">{{$child->title}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                        <ul class="mobile-site">
                            <li class="close-icon">
                                <a href="#" class="menu-a"><i class="fas fa-times"></i></a>
                            </li>
                            @foreach($menu as $item)
                                <div class="panel">
                                    <li>
                                        <a href="{{$item->link}}" class="menu-a" data-toggle="collapse"
                                           data-target="#collapseOne"
                                           aria-expanded="false" aria-controls="collapseOne">{{$item->title}}
                                            <span>{{$item->subtitle}}</span></a>
                                        @if(count($item->children) >0)
                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                                 data-parent="#accordion" style="">
                                                <ul>
                                                    @foreach($item->children as $child)
                                                        <li>
                                                            <a href="{{$child->link}}">{{$child->title}}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                    </li>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>
