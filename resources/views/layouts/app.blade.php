<!DOCTYPE html>
<html lang="ru-RU">
<head>
    @php($l = app()->getLocale())
    @php($setting = (array)$settings)
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'>
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#3db7e8">
    <link rel="shortcut icon" href="img/logo-head.png" type="image/x-icon">
    <title>{{$setting['sitename_'.$l]}}</title>

    <link href="/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href="/css/slick.css" type="text/css" rel="stylesheet">
    <link href="/css/gspeech.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
    <link href="/css/style.css" type="text/css" rel="stylesheet">
    <link href="/css/media.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    @stack('css')
</head>
<body>
@include('blocks.header')
<main>
    @include('blocks.baner')
    @include('blocks.category')
    @yield('content')
</main>
@include('blocks.footer')
<div class="fixed">
    <button class="big-button" id="toggle-assistance">
        <img src="img/phone-info.png">
        <i class="close">×</i>
    </button>
    <div class="buttons-group">
        <a href="#" class="small-button" data-toggle="modal">
            <img src="img/sms.png">
        </a>
        <a href="#" class="small-button" data-toggle="modal" data-target="#request-call">
            <img src="img/phone-icon.png">
        </a>
    </div>
    <div class="modal fade" id="request-call" tabindex="-1" role="dialog" aria-labelledby="request-call" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="POST">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Заказать звонок</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input class="form-control" placeholder="Ваше имя" name="name" id="callback-name" required="required">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Ваш номер телефона" name="phone" type="tel" id="callback-phone" required="required">
                        </div>
                    </div>
                    <div class="send-callback">
                        <button type="button" id="send_callback" class="btn btn-primary">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="side-nav">
    <a href="#" data-id="plus" class="fixed-item changefont">
        <span class="text-img">A+</span>
        <p>Большой шрифт сайта</p>
    </a>
    <a href="#" data-id="default" class="fixed-item changefont">
        <span class="text-img">Aa</span>
        <p>Стандартный шрифт сайта</p>
    </a>
    <a href="#" data-id="minus" class="fixed-item changefont">
        <span class="text-img">A-</span>
        <p>Меньший шрифт сайта</p>
    </a>
    <a href="#" class="fixed-item changecolor" data-id="minus">
        <img src="img/eyes-icon.png">
        <p>White and black mode</p>
    </a>
    <a href="#" class="fixed-item">
        <img src="img/volume-icon.png">
        <p>Звуковое сопровождение</p>
    </a>
    <a href="#" class="fixed-item">
        <img src="img/shine-icon.png">
        <p>Звуковое сопровождение</p>
    </a>
    <a href="#" class="fixed-item mobile-ver">
        <img src="img/phone-fixed-icon.png">
        <p>Мобильная версия</p>
    </a>
</div>
<div id="sound_container" class="sound_div sound_div_basic size_1 speaker_32" title="" style="">
    <div id="sound_text"></div>
</div>
<div id="sound_audio"></div>
<span style="position: absolute; margin-top: -50000px;" id="gs_tooltip">
<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/js/jqueryRotate.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/wow.min.js"></script>
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/js/script.js"></script>
<script src="/js/gs.js"></script>
<script src="/js/gspeech.js"></script>
<script src="/js/gspeech-pro.js"></script>
@stack('js')
</body>
</html>
