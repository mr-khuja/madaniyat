@extends('layouts.app')

@section('content')
    @php($l = app()->getLocale())

    <section class="section-advertising">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card wow flipInX">
                        <a href="#"><img src="/img/ing.png"></a>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="section-news wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="news-menu">
                        <ul class="nav tablist">
                            <li>
                                <button class="active" data-toggle="tab" href="#news">@lang('Новости')</button>
                            </li>
                            <li>
                                <button data-toggle="tab" href="#gover_news">@lang('Новости министерства')</button>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane news fade show active" id="news">
                                    <div class="news-block first-banner">
                                        <div class="news-row">
                                            @isset($news[0][0])
                                                <div class="news-item">
                                                    <div class="news-img">
                                                        <a href="/post/{{$news[0][0]->id}}">
                                                            <img src="{{$news[0][0]->image}}">
                                                        </a>
                                                        <div class="data-user">
                                                            <div>
                                                                <div class="text-botom">
                                                                    <p>
                                                                        <i class="far fa-calendar"></i>{{date('F', strtotime($news[0][0]->created_at)).' '.date('d', strtotime($news[0][0]->created_at)).', '.date('Y', strtotime($news[0][0]->created_at))}}
                                                                    </p>
                                                                    <p><i class="fas fa-eye"></i>{{$news[0][0]->views}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="news-text">
                                                        <h2>{{$news[0][0]->title}}</h2>
                                                        <p>{{$news[0][0]->short}}</p>
                                                        <a href="/post/{{$news[0][0]->id}}"><i
                                                                class="fas fa-chevron-right"></i></a>
                                                    </div>
                                                </div>
                                            @endisset
                                            @isset($news[0][1])
                                                <div class="news-column">
                                                    <div class="news-item">
                                                        <div class="news-img">
                                                            <a href="/post/{{$news[0][1]->id}}">
                                                                <img src="{{$news[0][1]->image}}">
                                                            </a>
                                                            <div class="data-user">
                                                                <div>
                                                                    <div class="text-botom">
                                                                        <p>
                                                                            <i class="far fa-calendar"></i>{{date('F', strtotime($news[0][1]->created_at)).' '.date('d', strtotime($news[0][1]->created_at)).', '.date('Y', strtotime($news[0][1]->created_at))}}
                                                                        </p>
                                                                        <p>
                                                                            <i class="fas fa-eye"></i>{{$news[0][1]->views}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="news-text">
                                                            <h2>{{$news[0][1]->title}}</h2>
                                                            <p>{{$news[0][1]->short}}</p>
                                                            <a href="/post/{{$news[0][1]->id}}"><i
                                                                    class="fas fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="news-item">
                                                        <div class="news-img">
                                                            <a href="/post/{{$news[0][2]->id}}">
                                                                <img src="{{$news[0][2]->image}}">
                                                            </a>
                                                            <div class="data-user">
                                                                <div>
                                                                    <div class="text-botom">
                                                                        <p>
                                                                            <i class="far fa-calendar"></i>{{date('F', strtotime($news[0][2]->created_at)).' '.date('d', strtotime($news[0][2]->created_at)).', '.date('Y', strtotime($news[0][2]->created_at))}}
                                                                        </p>
                                                                        <p>
                                                                            <i class="fas fa-eye"></i>{{$news[0][2]->views}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="news-text">
                                                            <h2>{{$news[0][2]->title}}</h2>
                                                            <p>{{$news[0][2]->short}}</p>
                                                            <a href="/post/{{$news[0][2]->id}}"><i
                                                                    class="fas fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endisset
                                        </div>
                                        @isset($news[0][3])
                                            <div class="news-item">
                                                <div class="news-img">
                                                    <a href="/post/{{$news[0][3]->id}}">
                                                        <img src="{{$news[0][3]->image}}">
                                                    </a>
                                                    <div class="data-user">
                                                        <div>
                                                            <div class="text-botom">
                                                                <p>
                                                                    <i class="far fa-calendar"></i>{{date('F', strtotime($news[0][3]->created_at)).' '.date('d', strtotime($news[0][3]->created_at)).', '.date('Y', strtotime($news[0][3]->created_at))}}
                                                                </p>
                                                                <p><i class="fas fa-eye"></i>{{$news[0][3]->views}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="news-text">
                                                    <h2>{{$news[0][3]->title}}</h2>
                                                    <p>{{$news[0][3]->short}}</p>
                                                    <a href="/post/{{$news[0][3]->id}}"><i
                                                            class="fas fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        @endisset
                                        @isset($news[0][4])
                                            <div class="news-item">
                                                <div class="news-img">
                                                    <a href="/post/{{$news[0][4]->id}}">
                                                        <img src="{{$news[0][4]->image}}">
                                                    </a>
                                                    <div class="data-user">
                                                        <div>
                                                            <div class="text-botom">
                                                                <p>
                                                                    <i class="far fa-calendar"></i>{{date('F', strtotime($news[0][4]->created_at)).' '.date('d', strtotime($news[0][4]->created_at)).', '.date('Y', strtotime($news[0][4]->created_at))}}
                                                                </p>
                                                                <p><i class="fas fa-eye"></i>{{$news[0][4]->views}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="news-text">
                                                    <h2>{{$news[0][4]->title}}</h2>
                                                    <p>{{$news[0][4]->short}}</p>
                                                    <a href="/post/{{$news[0][4]->id}}"><i
                                                            class="fas fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        @endisset
                                    </div>
                                </div>
                                <div class="tab-pane news fade show active" id="gover_news">
                                    <div class="news-block first-banner">
                                        <div class="news-row">
                                            @isset($news[1][0])
                                                <div class="news-item">
                                                    <div class="news-img">
                                                        <a href="/post/{{$news[1][0]->id}}">
                                                            <img src="{{$news[1][0]->image}}">
                                                        </a>
                                                        <div class="data-user">
                                                            <div>
                                                                <div class="text-botom">
                                                                    <p>
                                                                        <i class="far fa-calendar"></i>{{date('F', strtotime($news[1][0]->created_at)).' '.date('d', strtotime($news[1][0]->created_at)).', '.date('Y', strtotime($news[1][0]->created_at))}}
                                                                    </p>
                                                                    <p><i class="fas fa-eye"></i>{{$news[1][0]->views}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="news-text">
                                                        <h2>{{$news[1][0]->title}}</h2>
                                                        <p>{{$news[1][0]->short}}</p>
                                                        <a href="/post/{{$news[1][0]->id}}"><i
                                                                class="fas fa-chevron-right"></i></a>
                                                    </div>
                                                </div>
                                            @endisset
                                            @isset($news[1][1])
                                                <div class="news-column">
                                                    <div class="news-item">
                                                        <div class="news-img">
                                                            <a href="/post/{{$news[1][1]->id}}">
                                                                <img src="{{$news[1][1]->image}}">
                                                            </a>
                                                            <div class="data-user">
                                                                <div>
                                                                    <div class="text-botom">
                                                                        <p>
                                                                            <i class="far fa-calendar"></i>{{date('F', strtotime($news[1][1]->created_at)).' '.date('d', strtotime($news[1][1]->created_at)).', '.date('Y', strtotime($news[1][1]->created_at))}}
                                                                        </p>
                                                                        <p>
                                                                            <i class="fas fa-eye"></i>{{$news[1][1]->views}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="news-text">
                                                            <h2>{{$news[1][1]->title}}</h2>
                                                            <p>{{$news[1][1]->short}}</p>
                                                            <a href="/post/{{$news[1][1]->id}}"><i
                                                                    class="fas fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="news-item">
                                                        <div class="news-img">
                                                            <a href="/post/{{$news[1][2]->id}}">
                                                                <img src="{{$news[1][2]->image}}">
                                                            </a>
                                                            <div class="data-user">
                                                                <div>
                                                                    <div class="text-botom">
                                                                        <p>
                                                                            <i class="far fa-calendar"></i>{{date('F', strtotime($news[1][2]->created_at)).' '.date('d', strtotime($news[1][2]->created_at)).', '.date('Y', strtotime($news[1][2]->created_at))}}
                                                                        </p>
                                                                        <p>
                                                                            <i class="fas fa-eye"></i>{{$news[1][2]->views}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="news-text">
                                                            <h2>{{$news[1][2]->title}}</h2>
                                                            <p>{{$news[1][2]->short}}</p>
                                                            <a href="/post/{{$news[1][2]->id}}"><i
                                                                    class="fas fa-chevron-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endisset
                                        </div>
                                        @isset($news[1][2])
                                            <div class="news-item">
                                                <div class="news-img">
                                                    <a href="/post/{{$news[1][3]->id}}">
                                                        <img src="{{$news[1][3]->image}}">
                                                    </a>
                                                    <div class="data-user">
                                                        <div>
                                                            <div class="text-botom">
                                                                <p>
                                                                    <i class="far fa-calendar"></i>{{date('F', strtotime($news[1][3]->created_at)).' '.date('d', strtotime($news[1][3]->created_at)).', '.date('Y', strtotime($news[1][3]->created_at))}}
                                                                </p>
                                                                <p><i class="fas fa-eye"></i>{{$news[1][3]->views}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="news-text">
                                                    <h2>{{$news[1][3]->title}}</h2>
                                                    <p>{{$news[1][3]->short}}</p>
                                                    <a href="/post/{{$news[1][3]->id}}"><i
                                                            class="fas fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        @endisset
                                        @isset($news[1][3])
                                            <div class="news-item">
                                                <div class="news-img">
                                                    <a href="/post/{{$news[1][4]->id}}">
                                                        <img src="{{$news[1][4]->image}}">
                                                    </a>
                                                    <div class="data-user">
                                                        <div>
                                                            <div class="text-botom">
                                                                <p>
                                                                    <i class="far fa-calendar"></i>{{date('F', strtotime($news[1][4]->created_at)).' '.date('d', strtotime($news[1][4]->created_at)).', '.date('Y', strtotime($news[1][4]->created_at))}}
                                                                </p>
                                                                <p><i class="fas fa-eye"></i>{{$news[1][4]->views}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="news-text">
                                                    <h2>{{$news[1][4]->title}}</h2>
                                                    <p>{{$news[1][4]->short}}</p>
                                                    <a href="/post/{{$news[1][4]->id}}"><i
                                                            class="fas fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        @endisset
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="show-more">
                            <a href="/newz">@lang('Показать больше')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-news wow fadeIn" id="news-directions">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="news-heading">
                        <h3>@lang('Новости по направлениям')</h3>
                        <p>@lang('Здесь вы можете прочесть новости Министерства Культуры Республики Узбекистан по направлениям, таких как: театр музеи, шоу-бизнес и т.д')</p>
                    </div>
                    <div class="news-menu">
                        <ul class="nav menu-directions">
                            @foreach($newz as $key => $item)
                                <li class="@if($key == 0) active @endif">
                                    <button class="@if($key == 0) active @endif" data-toggle="tab"
                                            href="#cat-{{$item->id}}">{{$item['title_'.$l]}}</button>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">

                            @foreach($newz as $key => $item)
                                <div class="tab-pane news fade @if($key == 0) show active @endif"
                                     id="cat-{{$item->id}}">
                                    <div class="news-block">
                                        @foreach($item->news as $child)
                                            <div class="news-item">
                                                <div class="news-img">
                                                    <a href="/post/{{$child->id}}">
                                                        <img src="{{$child->image}}">
                                                    </a>
                                                    <div class="data-user">
                                                        <div>
                                                            <a class="category">{{$item['title_'.$l]}}</a>
                                                            <div class="text-botom">
                                                                <p>
                                                                    <i class="far fa-calendar"></i>{{date('F', strtotime($child->created_at)).' '.date('d', strtotime($child->created_at)).', '.date('Y', strtotime($child->created_at))}}
                                                                </p>
                                                                <p><i class="fas fa-eye"></i>{{$child->views}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="news-text">
                                                    <h2>{{$child->title}}</h2>
                                                    <p>{{$child->short}}</p>
                                                    <a href="/post/{{$child->id}}"><i
                                                            class="fas fa-chevron-right"></i></a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="show-more">
                            <a href="/news">@lang('Показать больше')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="news-subscribe">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="subscribe-form">
                        <div class="subscribe-heading wow fadeInUp">
                            <h5>@lang('подписаться на все') <span>@lang('Новости')</span></h5>
                        </div>
                        <form class=" wow fadeInUp">
                            <input type="email" name="text" placeholder="Email" class="input">
                            <input type="submit" name="" value="Подписаться" class="submit">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-news" id="news-for-directions">
        <div class="container">
            <div class="row">
                <div class="col-12 gallery-size-top">
                    <div class="news-heading">
                        <h3>@lang('Медия лента')</h3>
                        <p>@lang('Пресс служба Министерства Культуры Республики Узбекистан будет представлять вам в медиа ленте самые последние фото и видео в сфере культуры')</p>
                    </div>
                    <div class="news-menu">
                        <ul class="menu-directions nav">
                            <li>
                                <button data-toggle="tab" class="active" href="#img">@lang('Фото')</button>
                            </li>
                            <li>
                                <button data-toggle="tab" href="#video">@lang('Видео')</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content col-12">
                    <div class="tab-pane show fade active" id="img">
                        <div class="row">
                            @isset($photos[0]->image)
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="images-size">
                                        <a data-fancybox="gallery" href="{{$photos[0]->image}}">
                                            <img src="{{$photos[0]->image}}">
                                            <div class="search-icon"></div>
                                        </a>
                                    </div>
                                </div>
                            @endisset
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12 mobile-col">
                                <div class="row">
                                    @isset($photos[1]->image)
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="images-size">
                                                <a data-fancybox="gallery" href="{{$photos[1]->image}}">
                                                    <img src="{{$photos[1]->image}}">
                                                    <div class="search-icon"></div>
                                                </a>
                                            </div>
                                        </div>
                                    @endisset
                                    @isset($photos[2]->image)
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                            <div class="images-size">
                                                <a data-fancybox="gallery" href="{{$photos[2]->image}}">
                                                    <img src="{{$photos[2]->image}}">
                                                    <div class="search-icon"></div>
                                                </a>
                                            </div>
                                        </div>
                                    @endisset
                                    @isset($photos[3]->image)
                                        <div class="col-12 image-top ">
                                            <div class="images-size mt-0">
                                                <a data-fancybox="gallery" href="{{$photos[3]->image}}">
                                                    <img src="{{$photos[3]->image}}">
                                                    <div class="search-icon"></div>
                                                </a>
                                            </div>
                                        </div>
                                    @endisset
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="video">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12 mobile-col">
                                <div class="row">
                                    @foreach($videos as $key => $item)
                                        @if($key < 4)
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                <div class="images-size">
                                                    <a data-fancybox="video" href="{{$item->video}}">
                                                        <video src="{{$item->video}}"></video>
                                                        <div class="video-icon"></div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-12 mobile-col">
                                <div class="row">
                                    @foreach($videos as $key => $item)
                                        @if($key > 3)
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                <div class="images-size">
                                                    <a data-fancybox="video" href="{{$item->video}}">
                                                        <video src="{{$item->video}}"></video>
                                                        <div class="video-icon"></div>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="show-more">
                    <a href="/media">@lang('Показать больше')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="folk">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="folk-heading">
                        <h4>НАРОДНЫЕ АРТИСТЫ<span>РЕСПУБЛИКИ УЗБЕКИСТАН</span></h4>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="folk-heading">
                        <h4>ЗАСЛУЖЕННЫЕ АРТИСТЫ<span>РЕСПУБЛИКИ УЗБЕКИСТАН</span></h4>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 wow fadeInUp">
                    <a href="#" class="images-folk">
                        <img src="img/images-folk.png">
                    </a>
                    <div class="statistics">
                        <ul>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                        </ul>
                        <div class="neme-folk">
                            <a href="#">Озодбек Назарбеков</a>
                            <span>Певец</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 wow fadeInUp">
                    <a href="#" class="images-folk">
                        <img src="img/images-folk2.png">
                    </a>
                    <div class="statistics">
                        <ul>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                        </ul>
                        <div class="neme-folk">
                            <a href="#">Юлдуз Усмонова</a>
                            <span>Певец</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 wow fadeInUp">
                    <a href="#" class="images-folk">
                        <img src="img/images-folk3.png">
                    </a>
                    <div class="statistics">
                        <ul>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#" class="color"><i class="fas fa-star"></i></a>
                        </ul>
                        <div class="neme-folk">
                            <a href="#">Луиза Альхамова</a>
                            <span>Художник</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 wow fadeInUp">
                    <a href="#" class="images-folk">
                        <img src="img/images-folk4.png">
                    </a>
                    <div class="statistics">
                        <ul>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#" class="color"><i class="fas fa-star"></i></a>
                        </ul>
                        <div class="neme-folk">
                            <a href="#">Борчанинов Кирилл</a>
                            <span>Художественный руководитель оперной труппы</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 wow fadeInUp">
                    <div class="show-more">
                        <a href="#">Все артисты</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="young">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="news-heading young-item">
                        <h3>МОЛОДЫЕ ДЕЯТЕЛИ КУЛЬТУРЫ</h3>
                        <p>Здесь вы можете ознакомиться с творчеством молодых деятелей культуры Республики
                            Узбекистан</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 wow fadeInUp">
                    <a href="#" class="images-folk">
                        <img src="img/images-folk5.png">
                    </a>
                    <div class="statistics new-item">
                        <ul>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#" class="color"><i class="fas fa-star"></i></a>
                        </ul>
                        <div class="neme-folk">
                            <a href="#" href="#">Шамсиев Умаржон</a>
                            <span>Художник</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 wow fadeInUp">
                    <a href="#" class="images-folk">
                        <img src="img/images-folk6.png">
                    </a>
                    <div class="statistics new-item">
                        <ul>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#" class="color"><i class="fas fa-star"></i></a>
                        </ul>
                        <div class="neme-folk">
                            <a href="#">Ботир Яминов</a>
                            <span>Певец</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 wow fadeInUp">
                    <a href="#" class="images-folk">
                        <img src="img/images-folk7.png">
                    </a>
                    <div class="statistics new-item">
                        <ul>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#" class="color"><i class="fas fa-star"></i></a>
                        </ul>
                        <div class="neme-folk">
                            <a href="#">Райхон Ганиева</a>
                            <span>Певица</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 wow fadeInUp">
                    <a href="#" class="images-folk">
                        <img src="img/images-folk8.png">
                    </a>
                    <div class="statistics new-item">
                        <ul>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#"><i class="fas fa-star"></i></a>
                            <a href="#" class="color"><i class="fas fa-star"></i></a>
                        </ul>
                        <div class="neme-folk">
                            <a href="#">Жанисбек Пиязов</a>
                            <span>Художник</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 wow fadeInUp">
                    <div class="show-more news-item-more">
                        <a href="#">Все молодые деятели культуры </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="service-statistics">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="news-heading young-item service-statistics2 wow fadeInUp">
                        <h3>Статистика и интерактивные услуги</h3>
                        <p>Здесь вы можете воспользоваться интерактивными услугами и ознакомиться с краткими отчетами о
                            деятельности Министерства Культуры Республики Узбекистан</p>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-6 wow fadeInUp">
                            <a href="#" class="item-statistics">
                                <div class="bg_statistics"></div>
                                <div class="statistics-block">
                                    <img src="img/statistics-block-img.png">
                                    <p>Интерактивные услуги</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 wow fadeInUp">
                            <a href="#" class="item-statistics">
                                <div class="bg_statistics"></div>
                                <div class="statistics-block">
                                    <img src="img/statistics-block-img2.png">
                                    <p>Электронное правительство</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 wow fadeInUp">
                            <a href="#" class="item-statistics">
                                <div class="bg_statistics"></div>
                                <div class="statistics-block">
                                    <img src="img/statistics-block-img3.png">
                                    <p>Открытые данные</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 wow fadeInUp">
                            <a href="#" class="item-statistics">
                                <div class="bg_statistics"></div>
                                <div class="statistics-block">
                                    <img src="img/statistics-block-img4.png">
                                    <p>Статистика</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
