@extends('layouts.app')

@section('content')
    <section class="section-page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="page-content">
                        <h2 class="page-title wow fadeInUp">@lang('Новости')</h2>
                        <div class="page-body">
                            <div class="row">
                                <div class="page-body-content wow fadeInUp col-md-9">
                                    <div class="list-items">
                                        @if(count($news) > 0)
                                            @foreach($news as $news)
                                                <div class="list-item">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-6 ">
                                                            <div class="item-img">
                                                                <img src="{{$news->image}}">
                                                                <div class="data-user">
                                                                    <p>
                                                                        <i class="far fa-calendar"></i>{{date('F', strtotime($news->created_at)).' '.date('d', strtotime($news->created_at)).', '.date('Y', strtotime($news->created_at))}}
                                                                    </p>
                                                                    <p><i class="fas fa-eye"></i>{{$news->views}}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6">
                                                            <div class="item-content">
                                                                <div>
                                                                    <h2>
                                                                        <a href="/post/{{$news->id}}">{{$news->title}}</a>
                                                                    </h2>
                                                                    <p>{{$news->short}}</p>

                                                                </div>
                                                                <div class="more">
                                                                    <a href="/post/{{$news->id}}">
                                                                        @lang('Подробнее')
                                                                        <i class="fas fa-chevron-right"></i>
                                                                    </a>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            @foreach($data as $item)
                                                @foreach($item->news as $news)
                                                    <div class="list-item">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-6 ">
                                                                <div class="item-img">
                                                                    <img src="{{$news->image}}">
                                                                    <div class="data-user">
                                                                        <p>
                                                                            <i class="far fa-calendar"></i>{{date('F', strtotime($news->created_at)).' '.date('d', strtotime($news->created_at)).', '.date('Y', strtotime($news->created_at))}}
                                                                        </p>
                                                                        <p><i class="fas fa-eye"></i>{{$news->views}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="item-content">
                                                                    <div>
                                                                        <h2>
                                                                            <a href="/newz/{{$news->id}}">{{$news->title}}</a>
                                                                        </h2>
                                                                        <p>{{$news->short}}</p>

                                                                    </div>
                                                                    <div class="more">
                                                                        <a href="/newz/{{$news->id}}">
                                                                            @lang('Подробнее')
                                                                            <i class="fas fa-chevron-right"></i>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    </div>


                                    {{--                                    <div class="show-more">--}}
                                    {{--                                        <button>Показать больше</button>--}}
                                    {{--                                    </div>--}}

                                </div>
                                <div class="page-body-sidebar col-md-3">
                                    <div class="sidebar-block wow fadeInRight categories">
                                        <div class="sidebar-block-title">
                                            <h3>@lang('Categories')</h3>
                                        </div>
                                        <div class="sidebar-block-content">
                                            <ul>
                                                @foreach($data as $item)
                                                    <li>
                                                        <a href="/news/{{$item->id}}">{{$item->title}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
