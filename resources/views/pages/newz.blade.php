@extends('layouts.app')

@section('content')
    <section class="section-page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="page-content">
                        <h2 class="page-title  wow fadeInUp">@lang('Новости')</h2>
                        <div class="page-body">


                            <div class="list-items">
                                @foreach($data[0] as $item)
                                    <div class="list-item wow fadeInUp">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 ">
                                                <div class="item-img">
                                                    <img src="{{$item->image}}">
                                                    <div class="data-user">
                                                        <p>
                                                            <i class="far fa-calendar"></i>{{date('F', strtotime($item->created_at)).' '.date('d', strtotime($item->created_at)).', '.date('Y', strtotime($item->created_at))}}
                                                        </p>
                                                        <p><i class="fas fa-eye"></i>{{$item->views}}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="item-content">
                                                    <div>
                                                        <h2><a href="/post/{{$item->id}}">{{$item->title}}</a></h2>
                                                        <p>{{$item->short}}</p>

                                                    </div>
                                                    <div class="more">
                                                        <a href="/post/{{$item->id}}">
                                                            @lang('Подробнее')
                                                            <i class="fas fa-chevron-right"></i>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                            {{--                            <div class="show-more wow fadeInUp">--}}
                            {{--                                <button>Показать больше</button>--}}
                            {{--                            </div>--}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
