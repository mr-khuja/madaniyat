@extends('layouts.app')

@section('content')
    @php($l = app()->getLocale())

    <section class="section-page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="page-content inner-page">
                        <h2 class="page-title wow fadeInUp">{{$news->title}}</h2>
                        <div class="page-body">
                            <div class="row">
                                <div class="page-body-content wow fadeInUp col-md-9">
                                    <div class="page-image">
                                        <img src="{{$news->image}}">
                                    </div>

                                    <div class="page-text">{!! $news->body !!}</div>
                                    <div class="page-footer">
                                        <a href="#" class="print"><i class="fa fa-print"></i></a>
                                        <a href="#" class="like"><i class="fa fa-eye"></i> {{$news->views}}</a>
                                        <a href="#" class="heard"><i class="fa fa-heart"></i> {{$news->rating}}</a>
                                    </div>
                                </div>
                                <div class="page-body-sidebar col-md-3">
                                    <div class="sidebar-block wow fadeInRight categories">
                                        <div class="sidebar-block-title">
                                            <h3>@lang('Categories')</h3>
                                        </div>
                                        <div class="sidebar-block-content">
                                            <ul>
                                                @foreach($data as $item)
                                                    <li>
                                                        <a href="/news/{{$item->id}}">{{$item['title_'.$l]}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
