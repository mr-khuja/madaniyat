<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('setlocale/{locale}', 'HomeController@language')->name('lang');
Route::get('newz', 'HomeController@newz')->name('news');
Route::get('news/{id?}', 'HomeController@news')->name('news');
Route::get('post/{id}', 'HomeController@post')->name('post');

Route::group([
    'prefix' => 'admin',
//    'middleware' => ['auth', 'admin']
    'middleware' => ['auth']
], function () {

    Route::get('deletephoto/{id}', 'Admin\DataController@deletephoto');
    Route::match(['get', 'post'], 'settings', 'Admin\DataController@settings');
    Route::match(['get', 'post'], 'contacts', 'Admin\DataController@contacts');
    Route::match(['get', 'post'], 'socials', 'Admin\DataController@socials');
    Route::get('logout', 'Admin\DataController@logout');
    Route::get('/', 'Admin\DataController@home');

    Route::group(['prefix' => 'menu'], function () {
        $controller = 'Admin\MenuController@';
        Route::get('/', $controller . 'list')->name('menu');
        Route::match(['get', 'post'], 'create', $controller . 'create');
        Route::match(['get', 'post'], 'edit/{id}', $controller . 'edit');
        Route::match(['get', 'post'], 'translate/{id}/{lang}', $controller . 'translate');
        Route::get('delete/{id}', $controller . 'delete');
        Route::post('order', $controller . 'order');
    });
    Route::group(['prefix' => 'category'], function () {
        $controller = 'Admin\CategoryController@';
        Route::get('/', $controller . 'list')->name('category');
        Route::match(['get', 'post'], 'create', $controller . 'create');
        Route::match(['get', 'post'], 'edit/{id}', $controller . 'edit');
        Route::get('delete/{id}', $controller . 'delete');
        Route::post('order', $controller . 'order');
    });
    Route::group(['prefix' => 'baners'], function () {
        $controller = 'Admin\BanerController@';
        Route::get('/', $controller . 'list')->name('baners');
        Route::match(['get', 'post'], 'create', $controller . 'create');
        Route::match(['get', 'post'], 'edit/{id}', $controller . 'edit');
        Route::match(['get', 'post'], 'translate/{id}/{lang}', $controller . 'translate');
        Route::get('delete/{id}', $controller . 'delete');
    });
    Route::group(['prefix' => 'news'], function () {
        $controller = 'Admin\NewsController@';
        Route::get('/', $controller . 'list')->name('news');
        Route::match(['get', 'post'], 'create', $controller . 'create');
        Route::match(['get', 'post'], 'edit/{id}', $controller . 'edit');
        Route::match(['get', 'post'], 'translate/{id}/{lang}', $controller . 'translate');
        Route::get('delete/{id}', $controller . 'delete');
    });
    Route::group(['prefix' => 'newz'], function () {
        $controller = 'Admin\NewzController@';
        Route::get('/', $controller . 'list')->name('newz');
        Route::match(['get', 'post'], 'create', $controller . 'create');
        Route::match(['get', 'post'], 'edit/{id}', $controller . 'edit');
        Route::match(['get', 'post'], 'translate/{id}/{lang}', $controller . 'translate');
        Route::get('delete/{id}', $controller . 'delete');
    });
    Route::group(['prefix' => 'links'], function () {
        $controller = 'Admin\LinkController@';
        Route::get('/', $controller . 'list')->name('links');
        Route::match(['get', 'post'], 'create', $controller . 'create');
        Route::match(['get', 'post'], 'edit/{id}', $controller . 'edit');
        Route::match(['get', 'post'], 'translate/{id}/{lang}', $controller . 'translate');
        Route::get('delete/{id}', $controller . 'delete');
    });
    Route::group(['prefix' => 'filial'], function () {
        $controller = 'Admin\FilialController@';
        Route::get('/', $controller . 'list')->name('filial');
        Route::match(['get', 'post'], 'create', $controller . 'create');
        Route::match(['get', 'post'], 'edit/{id}', $controller . 'edit');
        Route::get('delete/{id}', $controller . 'delete');
    });
    Route::group(['prefix' => 'media'], function () {
        $controller = 'Admin\MediaController@';
        Route::get('/', $controller . 'list')->name('media');
        Route::match(['get', 'post'], 'photo', $controller . 'photo');
        Route::match(['get', 'post'], 'video', $controller . 'video');
        Route::get('delete/{type}/{id}', $controller . 'delete');
    });
    Route::group(['prefix' => 'pages'], function () {
        $controller = 'Admin\PagesController@';
        Route::get('/', $controller . 'list');
        Route::match(['get', 'post'], 'create', $controller . 'create');
        Route::match(['get', 'post'], 'edit/{id}', $controller . 'edit');
        Route::match(['get', 'post'], 'translate/{id}/{lang}', $controller . 'translate');
        Route::get('delete/{id}', $controller . 'delete');
    });
});
